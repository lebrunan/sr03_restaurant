package server;

import common.Point2D;
import common.Restaurant;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Restaurant [] restaurants = createRestaurantArray();

        Socket clientSocket = acceptClient();

        Point2D clientPosition = getClientPosition(clientSocket);

        Restaurant nearestRestaurant = getNearestRestaurant(restaurants, clientPosition);

        sendNearestRestaurantToClient(clientSocket, nearestRestaurant);
    }

    private static void sendNearestRestaurantToClient(Socket clientSocket, Restaurant nearestRestaurant) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
        objectOutputStream.writeObject(nearestRestaurant);
    }

    private static Restaurant getNearestRestaurant(Restaurant[] restaurants, Point2D clientPosition) {
        Restaurant nearestRestaurant = restaurants[0];
        for (Restaurant restaurant: restaurants) {
            if (restaurant.isNearer(nearestRestaurant,clientPosition)) {
                nearestRestaurant = restaurant;
            }
        }
        return nearestRestaurant;
    }

    private static Point2D getClientPosition(Socket clientSocket) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(clientSocket.getInputStream());
        return (Point2D) objectInputStream.readObject();
    }

    private static Socket acceptClient() throws IOException {
        System.out.println("Attente d'une connexion client ...");
        ServerSocket serverSocket = new ServerSocket(10080);
        Socket accept = serverSocket.accept();
        System.out.println("Client Connecté");

        return accept;
    }

    private static Restaurant[] createRestaurantArray() {
        return new Restaurant[]{
                new Restaurant("Philantrope", "01 01 01 01 01", new Point2D(1, 1)),
                new Restaurant("Picasso", "02 02 02 02 02", new Point2D(3, 8)),
                new Restaurant("Centre de recherche", "03 03 03 03 03", new Point2D(15, 3)),
                new Restaurant("Sun burger", "04 04 04 04 04", new Point2D(9, 9)),
        };
    }

}

