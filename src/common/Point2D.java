package common;

import java.io.Serializable;

public class Point2D implements Serializable {
    private int x;
    private int y;

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public double distanceFrom(Point2D clientPosition) {
        return Math.sqrt(Math.pow((this.x - clientPosition.getY()),2) + Math.pow((this.x - clientPosition.getY()),2));
    }
}
