package common;

import java.io.Serializable;

public class Restaurant implements Serializable {
    private String name;
    private String phone;
    private Point2D position;

    public Restaurant(String name, String phone, Point2D position) {
        this.name = name;
        this.phone = phone;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public Point2D getPosition() {
        return position;
    }

    public boolean isNearer(Restaurant restaurant, Point2D clientPosition) {
        return this.position.distanceFrom(clientPosition) < restaurant.getPosition().distanceFrom(clientPosition);
    }

    public String toString() {
        return
                "Nom du restaurant : " + name + "\n" +
                "Numéro de téléphone : " + phone + "\n" +
                "Position : " + position.getX() + position.getY();
    }
}
