package client;

import common.Point2D;
import common.Restaurant;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {


        Socket socket = connectToServer();
        Point2D clientPosition = getPositionFromClient();

        sendServerCurrentPosition(socket,clientPosition);
        Restaurant nearestRestaurant = receiveNearestRestaurant(socket);

        System.out.println(nearestRestaurant.toString());
    }

    private static Restaurant receiveNearestRestaurant(Socket socket) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
        return (Restaurant) objectInputStream.readObject();
    }

    private static Socket connectToServer() throws IOException {
        System.out.println("Connexion au serveur ...");
        Socket socket = new Socket("localhost", 10080);
        System.out.println("Client Connecté");
        return socket;
    }

    private static Point2D getPositionFromClient() {
        System.out.println("Client Connecté. Entrez votre position x");
        Scanner scanner = new Scanner(System.in);
        int xClient = scanner.nextInt();
        System.out.println("Entrez votre position y");
        int yClient = scanner.nextInt();
        return new Point2D(xClient,yClient);
    }


    private static void sendServerCurrentPosition(Socket socket, Point2D clientPosition) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectOutputStream.writeObject(clientPosition);
    }
}
